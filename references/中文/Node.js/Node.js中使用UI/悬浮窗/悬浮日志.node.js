// 设置在UI线程运行，但没有Activity页面
// 同时只能有一个引擎在UI线程运行，因此有多个悬浮窗需要用同一个引擎管理
"ui-thread";

const { createWindow, canDrawOverlays, manageDrawOverlays } = require('floating_window');
const { showToast } = require('toast');
const { R } = require('ui');

async function main() {
    // 检查悬浮窗权限
    if (!canDrawOverlays()) {
        showToast('没有悬浮窗权限');
        manageDrawOverlays();
        return;
    }

    // 创建悬浮窗
    const window = createWindow();
    // 从xml设置View
    // 暂时不支持list组件、自定义组件等特性，请等待后续更新
    window.setViewFromXml(`
        <vertical bg="#ffffff">
            <horizontal padding="8">
                <text text="Node.js: ${process.version}" textColor="#aa0000" textSize="16" width="0" layout_weight="1"/>                
                <img id="dragHandle" src="@drawable/ic_drag_handle_black_48dp" margin="4 0" bg="?selectableItemBackground"/>
                <img id="close" src="@drawable/ic_close_black_48dp" margin="4 0" bg="?selectableItemBackground"/>
            </horizontal>
            <globalconsole id="console" w="*" h="400"/>
        </vertical>
    `);
    // 获取用于拖拽的View
    const dragHandle = window.view.findView('dragHandle');
    // 启用拖拽手势，拖动该View可以拖动窗口
    window.enableDrag(dragHandle);

    // 获取关闭按钮
    const closeButton = window.view.findView('close');
    // 设置点击回调，暂时不支持view.on('click')的事件方式，请等待后续更新
    closeButton.setOnClickListener(() => {
        window.close();
        process.exit();
    });

    await window.show();
    $autojs.keepRunning();
}

main();