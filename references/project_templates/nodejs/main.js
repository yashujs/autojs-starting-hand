"nodejs";

const fs = require("fs");
const http = require("http");
const https = require("https");
const util = require("util");

const { showToast } = require("toast");
const $ui = require("ui");
const $accessibility = require("accessibility");

const $java = $autojs.java;
const context = $autojs.androidContext;

async function main() {
    showToast("Hello, World");
    console.log("Hello, World");
    console.log("versions:", process.versions);
}

main();
