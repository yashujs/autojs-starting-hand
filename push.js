var colors = require("colors");
const shell = require("shelljs");

/* ---------------主逻辑----------------------------------------------------------- */
let commitMessage;
process.argv.forEach((val, index) => {
  // console.log(`${index}: ${val}`);
  if (index == 2) {
    commitMessage = val.trim();
  }
});
console.log("推送到远程仓库:", commitMessage);
if (!commitMessage || commitMessage.length < 1) {
  let info = "推送到远程仓库时, 提交说明长度必须大于1, 推送到远程仓库命令: npm run push commitMessage";
  console.log(info.red);
  throw info;
}

let fullTime = getFullTime();
commitMessage = commitMessage + fullTime;
let result = shell
  .exec("git rm -r --cached references")
  .exec("git add .")
  .exec('git commit -m "' + commitMessage + '"')
  .exec("git push origin master")
  .exec("git checkout  -b " + commitMessage)
  .exec("git add .")
  .exec('git commit -m "' + commitMessage + '"')
  .exec("git push origin " + commitMessage)
  .exec("git checkout master");

if (result.code !== 0) {
  let info = "Error: 推送到远程仓库 failed";
  console.log(info.red);
  shell.exit(1);
}
/* -------------------------------------------------------------------------- */
function getFullTime() {
  var date = new Date();
  var retData = {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
    hour: date.getHours(),
    minutes: date.getMinutes(),
    seconds: date.getSeconds(),
  };
  let fullTime = `${retData.year}-${retData.month}-${retData.day}-${retData.hour}_${retData.minutes}_${retData.seconds}`;
  return fullTime;
}
