# autojs起手式

## 解决痛点

1. 有些人不知道自动补全怎么弄
2. 有些人代码老丢, 丢了就没了, 要及时把代码备份到码云的嘛

## 包含功能

1. 代码补全
    ![代码提示](img/automaticCompletion.jpg)

2. 自带参考代码, 提取的是 **autojs9.1.10** 自带的代码例子

3. 自动创建项目, 你在命令行输入 `npm run create project-name`, 就会创建新项目, config.js的projectFolder是项目所在文件夹

4. 自动创建远程仓库, config.js中的选项 associateRemoteRepository, 为true的话, 就会自动创建远程仓库, 并关联本地仓库

5. 自动上传文件到gitee, 你在命令行输入 `npm run push`, 就自动运行git的三个命令add, commit, push



## 环境
win10

## 使用前提
1. 电脑有nodejs环境
2. 电脑有git环境
3. 配置了码云的ssh密钥

## 使用步骤
1. 打开命令行, 安装依赖, 输入 `npm i`
2. 测试scripts功能是否正常, 命令行输入 `npm --silent run hello`
3. 想一个英文的项目名字, 比如 my-project
4. 命令行输入 `npm run create my-project`

## 创建的项目

![代码提示](img/newProjcet.png)

## 创建项目后, 怎样运行autojs脚本
1. vscode 打开刚才创建的项目文件, 在config.js中配置, 默认和本项目在同一个文件夹下
2. 在vscode中按F1, 输入autojs
3. 查找 运行项目 / 运行单文件
4. 点击 运行项目 / 运行单文件

## 快速打开项目文件夹
在vscode的命令行中, 输入 `code  文件夹路径`, 支持相对路径

## 快速打开命令行所在文件夹
`start .`

## 创建的新项目要push到码云

1. 在新项目中, 命令行输入 `npm i`, 安装依赖
2. 命令行中输入`npm run push commitMessage`, 执行推送命令

## 不想要每个新项目都 npm i 安装依赖
那么就全局安装依赖吧

NODE_PATH: NODE中用来寻找模块所提供的路径注册环境变量

查找NODE_PATH路径的两个方法

1. `npm root -g`
2. `npm config get prefix`

找到路径后, 新建系统变量(win10 系统属性/ 环境变量/ 新建)

变量名: NODE_PATH

变量值: C:\Users\Administrator\AppData\Roaming\npm\node_modules

全局安装脚手架依赖

`npm install colors npm-run-all request shelljs -g`

## Auto.js Pro如何连接VS Code插件
https://www.autojs.org/topic/3551/auto-js-pro%E5%A6%82%E4%BD%95%E8%BF%9E%E6%8E%A5vs-code%E6%8F%92%E4%BB%B6

## 获取码云token
个人主页/ 设置/ 安全设置/ 私人令牌/ 生成新令牌

## 警告

请严格保管你的token, 避免泄漏, 因为token的权限很大, 可以读写以及删除仓库

## 配置码云SSH公钥

[生成/添加SSH公钥 - Gitee.com](https://gitee.com/help/articles/4181)

## 相关教程

[autojs提取软件自带例子 · 语雀 (yuque.com)](https://www.yuque.com/yashujs/bfug6u/oeio7o)

[autojs给码云提交issue · 语雀 (yuque.com)](https://www.yuque.com/yashujs/bfug6u/ad7z0u)

## 您的打赏，会给我提供更大的前行动力，谢谢！

<center class="half">
    <img src="./vx.png" width="400"/>
    <img src="./zfb.png" width="400"/>
</center>
